#include "operations.h"
#include <stdlib.h>
#include <stdio.h>

void checkIndex(LinkedList* list, int index);

Node* newNode(int val){
    Node* n = malloc(sizeof(Node));
    n->val = val;
    n->next = NULL;
    return n;
}
LinkedList* createList(){
    LinkedList* list = malloc(sizeof(LinkedList));
    list->size = 0;
    list->first = NULL;
}
int getSize(LinkedList* list){
    return list->size;
}
int isListEmpty(LinkedList* list){
    return getSize(list) == 0;
}
void checkEmptyList(LinkedList* list){
    if(isListEmpty(list)){
        printf("Lista vazia!");
        exit(1);
    }
}
void addElemTail(LinkedList* list, Node* elem){
    if(list->first == NULL) 
        list->first = elem;
    else{
        getLastElem(list)->next = elem;
    }
    list->size++;
}

void addElem(LinkedList* list, Node* elem, int index){

    Node* p = getElem(list, index-1);
    elem->next = p->next;
    p->next = elem;
    list->size++;

}
void addElemHead(LinkedList* list, Node* elem){
    elem->next = list->first;
    list->first = elem;
    list->size++;
}
Node* getFirstElem(LinkedList* list){
    
    checkEmptyList(list);   

    return list->first;
}
Node* getLastElem(LinkedList* list){
    return getElem(list, list->size-1);
}
void checkIndex(LinkedList* list, int index){
    if(index > getSize(list) || index < 0){
        printf("Indice invalido. Programa finalizado!");
        exit(1);
    }
}
Node* getElem(LinkedList* list, int index){
    checkIndex(list, index);
    Node* p = list->first;
    while(index > 0){
        p = p->next;
        index--;
    }
    return p;
}
void removeElem(LinkedList* list, int index){
    
    Node* p = getElem(list, index-1);
    Node* n = p->next;
    
    p->next = n->next;
    free(n);

    list->size--;
    
}
void removeElemTail(LinkedList* list){
    removeElem(list, list->size-1);
}
void removeElemHead(LinkedList* list){
    
    checkEmptyList(list);   

    Node* h = list->first;
    list->first = h->next;
    free(h);
    list->size--;

}
void printList(LinkedList* list){
    Node* p = list->first;
    while(p != NULL){
        printf("%d\n",p->val);
        p = p->next;
    }
}
void attribList(LinkedList* source, LinkedList* destination){
    destination->size = source->size;
    Node* pSource = source->first;
    Node* pDestination = newNode(pSource->val);
    destination->first = pDestination;
    pSource = pSource->next;
    while(pSource != NULL){
        pDestination->next = newNode(pSource->val);
        pSource = pSource->next;
        pDestination = pDestination->next;
    }
}
void bubleSort(LinkedList* source, LinkedList* destination){
    attribList(source, destination);
    int i = destination->size;
    while(i){
        Node* p = destination->first;
        while(p->next != NULL){
            if(p->val > p->next->val){
                int temp = p->val;
                p->val = p->next->val;
                p->next->val = temp;
            }
            p = p->next;
        }
        i--;
    }
}
