typedef struct Node Node;
typedef struct LinkedList LinkedList;
  
struct Node{
    int val;
    Node* next;
};
struct LinkedList{
    int size;
    Node* first;
};

/* Create a node of type int */
Node* newNode(int val);


/* Starts an empty list */
LinkedList* createList();

/* Return the list's size*/
int getSize(LinkedList* list);

/* Return true if the list is empty */
int isListEmpty(LinkedList* list);

/* Add Element at the end of the list */
void addElemTail(LinkedList* list, Node* elem);

/* Add Element at the head of the list */
void addElemHead(LinkedList* list, Node* elem);

/* Add Element at the 'index' position of the list */
void addElem(LinkedList* list, Node* elem, int index);

/* Get the first element of the list */
Node* getFirstElem(LinkedList* list);

/* Get the last element of the list */
Node* getLastElem(LinkedList* list);

/*Return the element at the 'index' */
Node* getElem(LinkedList* list, int index);

/* Remove the element at position 'index' */
void removeElem(LinkedList* list, int index);

/* Remove Element at the end of the list */
void removeElemTail(LinkedList* list);

/* Remove Element at the head of the list */
void removeElemHead(LinkedList* list);

/* Print all elements from a list */
void printList(LinkedList* list);

/* Atributes all the values from a list to another  */
void attribList(LinkedList* source, LinkedList* destination);

void selectionSort(LinkedList* source, LinkedList* destination);

/* Buble Sort in a LinkedList  */
void bubleSort(LinkedList* source, LinkedList* destination);
