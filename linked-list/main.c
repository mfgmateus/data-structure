#include <stdio.h>
#include "operations.h"

int main(){
    
    LinkedList* l = createList();
    LinkedList* lCopy = createList();   
    addElemTail(l, newNode(100));
    addElemTail(l, newNode(2));
    addElemHead(l, newNode(50));
    addElem(l, newNode(23), 1);
    addElem(l, newNode(55), 2);
    addElem(l, newNode(53), 2);
    addElem(l, newNode(2), 3);
    
    printf("Lista de elementos\n");
    printList(l);

    removeElem(l, 1);

    printf("Lista de elementos\n");
    printList(l);

    removeElemTail(l);
    
    printf("Lista de elementos\n");
    printList(l);

    removeElemHead(l);

    printf("Lista de elementos\n");
    printList(l);

    attribList(l, lCopy);

    printf("Lista cópia\n");
    printList(lCopy);

    LinkedList* lSorted = createList();
    bubleSort(l, lSorted);

    printf("Lista ordenada\n");
    printList(lSorted);
    
    printf("Numero de elementos: %d\n",getSize(l));
    printf("Primeiro elemento:   %d\n",getFirstElem(l)->val);
    printf("Segundo elemento:    %d\n",getElem(l,1)->val);
    printf("Ultimo elemento:     %d\n",getLastElem(l)->val);

    return 0;
}

